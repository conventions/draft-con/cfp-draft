/*
This script apply labels to every issue 
depending of the checkboxes value in the body of the issue

*/

import { Gitlab } from '@gitbeaker/node'

import { config } from 'dotenv'
config()

const api = new Gitlab({
  token: process.env["GITLAB_TOKEN_ADMIN"],
  host: process.env["GITLAB_HOST"],
})

function exitIfError(error, topic) {
  if(error) { console.log(`😡[${topic}]`, error.message); process.exit(1); }
}

async function getAllOpenedIssues() {
  try {
    let issues = await api.Issues.all({projectId: process.env["PROJECT_ID"]})
    return [null, issues.filter(issue => issue.state==="opened")]
  } catch (error) {
    return [error, null]
  }
}

async function getAllLabelsOfMainGroup() {
  try {
    let labels = await api.GroupLabels.all(process.env["MAIN_GROUP_ID"])
    return [null, labels]
  } catch (error) {
    return [error, null]
  }
}

// Start

let [errorIssues, openedIssues] = await getAllOpenedIssues()
exitIfError(errorIssues, "issues")

let [errorLabels, labels] = await getAllLabelsOfMainGroup()
exitIfError(errorLabels, "labels")


openedIssues.forEach(issue => { // issue.description
  
  console.log(issue.id, issue.iid, issue.title)

  labels.forEach(label => {
    var isScopedLabel = label.name.includes("::") ? true : false
    var labelName = isScopedLabel ? label.name.split("::")[1] : label.name
    //var labelScope = isScopedLabel ? label.name.split("::")[0] : ""
    
    var labelsToAdd = []
    if(issue.description.toLowerCase().includes(`- [x] ${labelName}`)) {
      console.log("👋 label to apply:", label.name)
      labelsToAdd.push(label.name)
    }
    // I cannot use `await` inside forEach
 
    let removeLabels = () => api.Issues.edit(
      process.env["PROJECT_ID"], issue.iid, 
      {labels:""}
    )
    let addLabels = () => api.Issues.edit(
      process.env["PROJECT_ID"], issue.iid, 
      {add_labels: labelsToAdd.join(",")}
    )

    removeLabels().then(addLabels).catch(error => { console.log(error)})

  })

})


