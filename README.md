# CFP Draft-con

**Remarks**:
- If you add (or update/remove) a choice (checkbox) to the template issue, you must reflect it in the labels' list of the main group (if you're using scoped labels, `scope::value`, the choice in the body is only `- [ ] value`)

